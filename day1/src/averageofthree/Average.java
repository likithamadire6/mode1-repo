/*Write a Java program that takes three numbers as input to calculate and print the average of the numbers
 * 
 * */

package averageofthree;

import java.util.Scanner;

class Average {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter a firstnum : ");

		int firstnum = sc.nextInt();

		System.out.print("Enter a second num : ");

		int secondnum = sc.nextInt();

		System.out.print("Enter a third num : ");

		int thirdnum = sc.nextInt();

		System.out.println("average of given numbers is " + ((firstnum + secondnum + thirdnum) / 3));

	}
}
