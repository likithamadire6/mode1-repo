/**Program to find longest word in sentence.
 *
 * @author likhitha.m
 *
 */
 package longstring;

import java.util.Scanner;

public class LongestString {

	public static void main(String[] args) {

		Scanner sc= new Scanner(System.in);
		
		System.out.println("Enter the Statement ");
		
		String statement=sc.nextLine();
		String[] word = statement.split(" ");
		String longString = "";
		for(int i=0;i<=word.length;i++) {
				if (word[i].length()>=longString.length()) {
					 longString=word[i];
					 
				}
		}
		System.out.println("longest word in sentence is " + longString);
			
		sc.close();
	}
		


}
