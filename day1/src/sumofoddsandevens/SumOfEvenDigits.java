package sumofoddsandevens;

public class SumOfEvenDigits {
	public int checkSum(int num)
	{
		int n=0, sum=0;
		while(num!=0)
		{
			n = num % 10;
			num = num / 10;
			if(n%2==0)
			{
	            
		     sum = sum + (n*n);
			}
		}
		
		return sum;
	}

}
