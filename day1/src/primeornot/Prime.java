package primeornot;

import java.util.Scanner;

public class Prime {

  public static void main(String[] args) {

	  Scanner sc = new Scanner(System.in);
	  System.out.println("enter the number to check prime or not : ");
    int num = sc.nextInt();
    sc.close();
    boolean notprime = false;
    for (int i = 2; i <= num / 2; ++i) {
      
      if (num % i == 0) {
        notprime = true;
        break;
      }
    }

    if (!notprime)
      System.out.println(num + " is a prime number.");
    else
      System.out.println(num + " is not a prime number.");
  }
}