package swapofnumbers;

public class Swap {

	
	
	public static void main(String[] args) {
     
		int x= 10;
		int y= 20;
		
		System.out.print( "before swap x is  " + x);
		System.out.println( " and  y is " +y);
		
		int temp =x;
		x=y;
		y=temp;
		
		System.out.print( "after swap x is " + x);
		System.out.println( " and y is  " +y);
		
	}

}
