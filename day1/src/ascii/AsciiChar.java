/*6. Write a Java program to print the ascii value of a given character. 

*@author lihitha.m
*/
package ascii;

import java.util.Scanner;

public class AsciiChar {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the input to know the ascii value of it  :");
		char ch = sc.next().charAt(0);

		int ascii = ch;

		System.out.println("The ASCII value of " + ch + " is: " + ascii);

		sc.close();
	}
}