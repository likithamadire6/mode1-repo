package inheritance;

public class A {
	int empNo;
	String empName;
	public int getEmpNo() {
		return empNo;
	}
	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public void display() {
		System.out.println("Display in parent student no " + this.getEmpNo());
		System.out.println("Display in parent student name " +this.getEmpName());
		
     
}

}
