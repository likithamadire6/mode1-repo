package inheritance;

public class B extends A {
	String branch;

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}
	public void display() {
		System.out.println("Display in derived student No " + super.getEmpNo());
		System.out.println("Display in derived student name " + super.getEmpName());
		System.out.println("Display in derived branch " + this.branch);
}
}
