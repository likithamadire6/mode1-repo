package inheritance;


public class Inherit {

	public static void main(String[] args) {
		A employee=new A();
		employee.setEmpNo(123);
		employee.setEmpName("kalyani");
		employee.display();
		
		B employee1=new B();
		employee1.setEmpNo(456);
		employee1.setEmpName("Deepthi");
		employee1.setBranch("CSe");
		employee1.display();
		
		A obj =new B();
		obj.setEmpNo(66);
		obj.display();
	


	}

}
