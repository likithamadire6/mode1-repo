package Vowelcount;

import java.util.Scanner;
public class VowelNum {
   public static void main(String args[]){
      int count = 0;
      System.out.println("Enter a sentence :");
      Scanner sc = new Scanner(System.in);
      String sentence = sc.nextLine();

      for (int i=0 ; i<sentence.length(); i++){
         char VC = sentence.charAt(i);
         if(VC == 'a'|| VC == 'e'|| VC == 'i' ||VC == 'o' ||VC == 'u'){
            count ++;
         }
      }
      System.out.println("Number of vowels in the given sentence is "+count);
   }
}

